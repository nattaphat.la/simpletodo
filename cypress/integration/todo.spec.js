describe('Todo app Test', function() {
  beforeEach(function() {
    cy.visit('http://localhost:8080')
    cy.get('[data-testid=todo-input]').type('Hello world 1 {enter}')
    cy.get('[data-testid=todo-input]').type('Hello world 2 {enter}')
    cy.get('[data-testid=todo-input]').type('Hello world 3 {enter}')
    cy.get('[data-testid=todo-container] > :nth-child(2) > input').click();
  })
  it('Should be able to add item', function() {
    cy.get('[data-testid=todo-container]').children().should('have.length', 3)
    cy.get('[data-testid=todo-input]').type('Hello world 4 {enter}')
    cy.get('[data-testid=todo-container]').children().should('have.length', 4)
  })
  it('all filter should show all todo item', function() {
    // expect(true).to.equal(true)
    // cy.get('[data-testid=todo-container] > :nth-child(2) > input').click();
    cy.get('[data-testid=todo-container]').children().should('have.length', 3)
  })
  it('completed filter should show all todo item', function() {
    // expect(true).to.equal(true)
    cy.get('[data-testid=filter-completed] > input').click();
    cy.get('[data-testid=todo-container]').children().should('have.length', 1)
  })
  it('incompleted filter should show all todo item', function() {
    // expect(true).to.equal(true)
    cy.get('[data-testid=filter-incompleted] > input').click();
    cy.get('[data-testid=todo-container]').children().should('have.length', 2)
  })
})